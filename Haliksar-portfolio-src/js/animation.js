$(document).ready(function () {
    let imagesCount = $('img').length,
        loadedImg = 0;
    for (let i = 0; i < imagesCount; i++) {
        const imgCopy = new Image();
        imgCopy.src = document.images[i].src;
        imgCopy.onload = img_load;
        imgCopy.onerror = img_load;
    }

    function img_load() {
        loadedImg++;
        if (loadedImg === imagesCount) {
            $('body').addClass('loaded');
        }
    }

    let slide = 0;
    $(`#${slide}`).trigger('click');
    setInterval(function run() {
        if ($(`#${slide}`).prop('checked') === true) {
            slide++;
        }
        $(`#${slide}`).trigger('click');
        if (slide >= 4) {
            slide = 0;
        }
    }, 2500);

    $('.check_class').on('click', function () {
        $('input.check_class').not(this).prop('checked', false);
        $('#slider-wrapper > div').addClass('hidden');
        switch ($(this).attr("id")) {
            case '0':
                $('#slider-wrapper > :nth-child(1)').removeClass('hidden');
                slide = 0;
                break;
            case '1':
                $('#slider-wrapper > :nth-child(2)').removeClass('hidden');
                slide = 1;
                break;
            case '2':
                $('#slider-wrapper > :nth-child(3)').removeClass('hidden');
                slide = 2;
                break;
            case '3':
                $('#slider-wrapper > :nth-child(4)').removeClass('hidden');
                slide = 3;
                break;
            case '4':
                $('#slider-wrapper > :nth-child(5)').removeClass('hidden');
                slide = 4;
                break;
        }
    });


    $('#works1, #works3').addClass('hidden').viewportChecker({
        classToAdd: "visible animated fadeInRight",
    });
    $('#works2, #works4').addClass('hidden').viewportChecker({
        classToAdd: "visible animated fadeInLeft",
    });
    $('.works-image').addClass('hidden').viewportChecker({
        classToAdd: "visible animated zoomIn",
    });
});

window.addEventListener("load", function () {
        bControll("#menubox", "#openMenu", "#close", "#aboutbox", "#openAbout", "#closeAbout");
});

async function bControll(menubox, open, close, aboutbox, openAbout, closeAbout) {
        var elem = typeof menubox === "string" ? document.querySelector(menubox) : menubox,
            open = typeof open === "string" ? document.querySelector(open) : open,
            close = typeof close === "string" ? document.querySelector(close) : close,
            elemAbout = typeof aboutbox === "string" ? document.querySelector(aboutbox) : aboutbox,
            openAbout = typeof openAbout === "string" ? document.querySelector(openAbout) : openAbout,
            closeAbout = typeof closeAbout === "string" ? document.querySelector(closeAbout) : closeAbout;
        open.addEventListener("click", function () {
            elem.style.display = "block";
            elem.style.visibility = "visible";
            $('.right-fix-menu, .left-fix-menu, #content-menu-box, #logo-menu, #main, #anchor').addClass('animated');
            $('.right-fix-menu').addClass('slideInRightMy');
            $('.left-fix-menu').addClass('slideInLeftMy');
            $('#content-menu-box').addClass('fadeInLeft').removeClass('fadeOutRight');
            $('#logo-menu').addClass('fadeIn').removeClass('fadeOut');
            $('#main, #anchor').addClass('f-blurIn').removeClass('f-blurOut');
            $('#header-flex-box').addClass('hidden');
        });
        close.addEventListener("click", function () {
            $('.right-fix-menu').removeClass('slideInRightMy').addClass('slideOutRightMy');
            $('.left-fix-menu').removeClass('slideInLeftMy').addClass('slideOutLeftMy');
            $('#content-menu-box').removeClass('fadeInLeft').addClass('fadeOutRight');
            $('#logo-menu').removeClass('fadeIn').addClass('fadeOut');
            $('#header-flex-box').removeClass('hidden');
            $('#main, #anchor').removeClass('f-blurIn').addClass('f-blurOut');
            setTimeout(function () {
                elem.style.visibility = "hidden";
                elem.style.display = "none";
            }, 600);
        });
        openAbout.addEventListener("click", function () {
            elemAbout.style.display = "block";
            elemAbout.style.visibility = "visible";
            $('#aboutbox').addClass("animated fadeIn").removeClass('fadeOut');
            $('#main, #anchor, #header-flex-box').addClass("animated f-blurIn").removeClass('f-blurOut');
        });
        closeAbout.addEventListener("click", function () {
            $('#aboutbox').removeClass('fadeIn').addClass('fadeOut');
            $('#main, #anchor, #header-flex-box').removeClass('f-blurIn').addClass('f-blurOut');
            setTimeout(function () {
                elemAbout.style.visibility = "hidden";
                elemAbout.style.display = "none";
            }, 600);
        });
}